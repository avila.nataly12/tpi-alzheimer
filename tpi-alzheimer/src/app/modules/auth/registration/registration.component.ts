import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Usuario } from '../../../interfaces/usuario';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  registerForm: FormGroup = this.fb.group({
    nombre: ['', [Validators.required]],
    apellido: ['', [Validators.required]],
    dni: ['', [Validators.required]],
    telefono: ['', [Validators.required]],
    rol: ['', [Validators.required]],
    email: ['', [Validators.required]],
    contrasena: ['', [Validators.required]],
    direccion: ['', [Validators.required]]
  });

  constructor(private fb: FormBuilder,
              private authService: AuthService ) { }

  ngOnInit(): void {
  }

  register() {
    const user: Usuario = this.registerForm.value;

    this.authService.register( user )
      .subscribe(data => {
        console.log(data);
      }, err => {
        console.log(err.error.mensaje);
      });
  }


}
