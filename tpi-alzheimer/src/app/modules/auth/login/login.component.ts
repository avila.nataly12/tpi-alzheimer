import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/interfaces/usuario';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = this.fb.group({
    email: ['test1@test.com', [Validators.required, Validators.email]], // TODO: cambiar validacion por expresion regular
    contrasena: ['123456', [Validators.required, Validators.minLength(6)]]
  }); 

  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private router: Router) { }

  ngOnInit(): void {
  }

  login() {
    const {email, contrasena} = this.loginForm.value;

    this.authService.login(email, contrasena)
      .subscribe(data => {
        console.log('Usuario logueado');
      }, err => {
        console.log(err.error.mensaje);
      })
  }

}
