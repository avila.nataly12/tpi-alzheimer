import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GaleriaRoutingModule } from './galeria-routing.module';
import { GaleriaComponent } from './galeria.component';
import { SharedModule } from '../../components/shared/shared.module';


@NgModule({
  declarations: [
    GaleriaComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    GaleriaRoutingModule,

  ]
})
export class GaleriaModule { }
