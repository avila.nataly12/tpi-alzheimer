import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Usuario } from 'src/app/interfaces/usuario';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient,
              private router: Router) { }

  register(user: Usuario): Observable<Usuario> {
    const url = `${this.baseUrl}`;
    const body = {user};

    return this.http.post<Usuario>(url, body);
  }

  login(email: string, contrasena: string) {
    const url = `${this.baseUrl}login`;
    const body = {email, contrasena};

    return this.http.post(url, body);
  }
  
}
