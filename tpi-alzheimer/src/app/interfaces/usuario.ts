export interface Usuario {
    nombre: string,
    apellido: string,
    dni: string,
    telefono: string,
    rol: string, 
    email: string,
    contrasena: string, 
    direccion: string,
    
}